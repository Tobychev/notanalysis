#encoding: utf8
import numpy as np
import scipy.optimize as so
import dirtyscience as ds
import lineutil as lu
import matplotlib.pyplot as pl
pl.close("all")
pixel_scale = 0.19 #arcppx

gr7cpix = (ds.gr7aps[:,2]-ds.gr7aps[5,2])*pixel_scale
gr8rpix = (ds.gr8aps[:,2]-ds.gr8aps[5,2])*pixel_scale

#pl.plot(ds.gr7aps[:,1],ds.gr7aps[:,2],'ro')
#pl.plot(ds.gr8aps[:,1],ds.gr8aps[:,2],'bx')
#pl.xlabel("Aperture start column")
#pl.ylabel("Aperture end column")

### Start N2 ###
Nlow = 7.173;Nhigh=8.845 # Limits of calibration
metN2,dmetN2 = lu.N2(ds.Ha8,ds.dHa8,ds.NII8,ds.dNII8)
metN2 = np.insert(metN2,0,metN2[0])
dmetN2 = np.insert(dmetN2,0,np.nan)
gr8rpix = np.insert(gr8rpix,0,gr8rpix[0]-4)

#lu.stairs(pl,gr8rpix,metN2,dmetN2,title="N2 in Grism 8",ylabel="12+log(O/H)",xlabel="arcsec",fig=1)

#Extra formating
scaleHa = 0.3*ds.Ha8/np.max(ds.Ha8)+8.75
#pl.plot(gr8rpix[1:],scaleHa,"-.")
#pl.plot(gr8rpix,gr8rpix*0+Nhigh,"--")

metN2gr7,dmetN2gr7 = lu.N2(ds.Ha7,ds.dHa7,ds.NII7,ds.dNII7)
#lu.stairs(pl,gr7cpix,metN2gr7+.2,dmetN2gr7,title="N2 Grism 7 ",ylabel="12+log(O/H)",xlabel="arcsec")

scaleHa = 0.4*ds.Ha7/np.max(ds.Ha7[~np.isnan(ds.Ha7)])+8.7
#pl.plot(gr7cpix,scaleHa,"-.")
#pl.plot(gr7cpix,gr7cpix*0+Nhigh,"--")
### End N2 ###

### Start R23 ###
R23low = 7
metR23,dmetR23 = lu.R23(ds.Hb,ds.dHb,ds.OII,ds.dOII,ds.OIIIc,ds.dOIIIc)
#lu.stairs(pl,gr7cpix,metR23,dmetR23,title="R23 in Grism 7",ylabel="12+log(O/H)",xlabel="arcsec")

scaleHa = 1.4*ds.Ha7/np.max(ds.Ha7[~np.isnan(ds.Ha7)])+8
#pl.plot(gr7cpix,scaleHa,"-.")
#pl.plot(gr7cpix,gr7cpix*0+R23low,"--")
### End R23 ###

### O3N2 ###
metO3N2, dmetO3N2 = lu.O3N2(ds.Ha7,ds.dHa7,ds.Hb,ds.dHb,ds.NII7,ds.dNII7,ds.OIIIc,ds.dOIIIc)

#lu.stairs(pl,gr7cpix,metO3N2,dmetO3N2,title="O3N2 in Grism 7",ylabel="12+log(O/H)",xlabel="arcsec")
#scaleHa = 5*ds.Ha7/np.max(ds.Ha7[~np.isnan(ds.Ha7)])+2
#pl.plot(gr7cpix,scaleHa,"-.")
### O3N2 ###

### Start Dust ###
Ebv, dEbv = lu.Ebv(ds.Ha7,ds.dHa7,ds.Hb,ds.dHb)
lu.stairs(pl,gr7cpix,Ebv,dEbv,title="Ebv by H-alpha/H-beta",xlabel="arcsec")

scaleHa = 1.4*ds.Ha7/np.max(ds.Ha7[~np.isnan(ds.Ha7)])
pl.plot(gr7cpix,scaleHa,"-.")
### End Dust ###


#SII lines #
rSII = ds.SIIa/ds.SIIb
drSII = np.sqrt( (ds.dSIIa/ds.SIIb)**2 + ((ds.SIIa/ds.SIIb**2)**ds.dSIIb )**2 )

