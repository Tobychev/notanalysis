#encoding: utf-8
import pyfits as fits
import numpy as np
import os

#stddir = "../fluxstandards/"
stddir = "../datarep/fluxstandards/"
extrdir = "extracted/standards/"
gr8dir = "extracted/grism8/"
insdir = "instrument/"

#Make calibration frame
def makeCalFrame(name,lines,instr):
    line = instr["flux"]
    imgdat = np.ones( (lines,len(line))) * line
    img = fits.PrimaryHDU(imgdat)
    #Set various header fields pyspeckit demand
    img.header["CRPIX1"] = 1.0
    img.header["CRVAL1"] = instr["lam"][0]
    img.header["CDELT1"] = np.diff(instr["lam"]).mean()
    img.header["CUNIT1"] = 'angstrom'
    img.header["BUNIT"]  = 'erg/s/cm^2/Ang'
    img.writeto(insdir+name+".fits")

#Make copy of extract without header
def makeheadless(mapp,name):
	lines = 0
	with open(mapp+name) as fil:
		while fil.readline()[0] not in "1234567890":
			pass
		l = len(fil.readline())
		pos = fil.tell()
		fil.seek(pos-2*l)
		with open(mapp+"hl"+name,"w") as out:
			for line in fil:
				out.write(line)

def jy2flam (jy,lam,djy=None):
	"""
		converts /Hz to /AA 
	"""
	c = 29979245800.    # speed of light in cgs
	flam=jy*1e-23*(c*1.e8)/lam**2
	if djy==None: 
		return flam
	else:
		dflam = djy*1e-23*(c*1.e8)/lam**2
		return flam, dflam

def saveInstrument(name, inst):
	with open(insdir+name,"w") as fil:
		np.savez(fil,inst["lam"],inst["flux"])	

def openInstrument(name):
	with open(insdir+name) as fil:
		tmp = np.load(fil)
		return {"lam":tmp["arr_0"],"inst":tmp["arr_1"]}

def listExtracted():
	print os.listdir(extrdir)

def listStandard():
	print os.listdir(stddir)

def openExtractStd(name):	
	return openextract(extrdir,name)

def openExtractGrism8(name):
	return 	openextract(gr8dir,name)

def openextract(mapp,fil):
	try:
		tmp = np.loadtxt(mapp+fil)
	except ValueError:
		try:
			tmp = np.loadtxt(mapp+"hl"+fil)
		except IOError:
			makeheadless(mapp,fil)
			tmp = np.loadtxt(mapp+"hl"+fil)
	lam = tmp[:,0]
	flux = tmp[:,1]

	return {"lam":lam,"flux":flux}

def openStandard(name,unit="Jy"):
	try:
		tmp = np.loadtxt(stddir+name)
	except:
		raise Exception("Error opening file "+stddir+name)
	lam = tmp[:,0]
	flux = tmp[:,1]

	if unit == "Jy":
		flam = jy2flam(flux,lam)
	elif unit == "muJy":
		flam = jy2flam(flux*1e-6,lam)
	else:
		raise Exception("Unit conversion from {} not implemented".format(unit))

	return {"lam":lam,"flux":flam}

def amplitude(fitres):
	names = ["Ap","NIIa","H-alpha","NIIb","SIIa","SIIb"]
	line = "{:4s} {:18s} {:18s} {:18s} {:18s} {:18s}"
	print line.format(*names)
	for row in fitres:
#		data = [str(row[0]),
#				str(row[1]["unknown_1"]["flux"]),
#				str(row[1]["unknown_2"]["flux"]),
#				str(row[1]["unknown_3"]["flux"]),
#				str(row[1]["unknown_4"]["flux"]),
#				str(row[1]["unknown_5"]["flux"])
#			   ]
		print row[1].keys()
#		print line.format(*data) 
