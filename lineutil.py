#encoding: utf8
import matplotlib.pyplot as pl
import numpy as np

def stairs(plot,x,y,dy=[],title="",xlabel="",ylabel="",fig=None):
	off = np.diff(x).mean()/2.0	
	plot.figure(fig)
	plot.step(x+off,y)
	if len(y) == len(dy):
		plot.errorbar(x,y,dy,fmt=None)
	plot.title(title)
	plot.xlabel(xlabel)
	plot.ylabel(ylabel)
	return plot

def SIIfit(nt,SIIrat):
	return 1.49*((1+3.77*nt)/(1+12.8*nt)) - SIIrat

def OIIIfit(t,N,OIIIrat):
	return 7.76*t**(-0.05)*np.exp(3.297/t)*((1+0.000384*N)/(1+0.0462*N))-OIIIrat

def N2(Ha,dHa,NII,dNII):
	N2 = NII/Ha
	dN2 = np.sqrt( (dNII/Ha)**2 + (dHa*(NII/Ha**2))**2 )
	return ( index(N2,9.263,0.836), dindex(N2,dN2,0.836) )

def R23_low(Hb,dHb,OII,dOII,OIII,dOIII):
	r23 = (OII+1.333*OIII)/Hb
	dr23 = np.sqrt( (dOII/Hb)**2 + (dOIII*1.3333/Hb)**2
	              + (dHb*(OII +1.3333*OIII)/Hb**2)**2)
	return ( index(r23,6.465,1.401), dindex(r23,dr23,1.401) )


def R23(Hb,dHb,OII,dOII,OIII,dOIII):
	r23 = (OII+1.333*OIII)/Hb
	dr23 = np.sqrt( (dOII/Hb)**2 + (dOIII*1.3333/Hb)**2
	              + (dHb*(OII +1.3333*OIII)/Hb**2)**2)

	sel = dr23/r23 < 1	
	met = []
	dmet = []
	for I,kvot in enumerate(r23):
		#poly1d can't handle nan
		if not sel[I]:
			met.append(np.nan)
			dmet.append(np.nan)
			continue
		metn = np.poly1d( [-.230, 4.806, -32.78, 73.13-np.log10(kvot)])
		#Störningsräkning
		pmet = np.poly1d([-.230, 4.806, -32.78, 73.13-np.log10(kvot+dr23[I])])
		mmet = np.poly1d([-.230, 4.806, -32.78, 73.13-np.log10(kvot-dr23[I])])
		varmet = mmet.r[0] - pmet.r[0]
		met.append(metn.r[0])
		dmet.append(varmet)
	return met,dmet

def Ebv(Ha,dHa,Hb,dHb):
	kHa = 2.4545; kHb = 3.5178
	Robs = Ha/Hb
	dRobs = np.sqrt( (dHa/Hb)**2 + (dHb*(Ha/Hb**2))**2 )
	c = 2.5/(kHa-kHb)
	return ( index(2.86/Robs,0,c), dindex(Robs,dRobs,c) )

def index(r,a,b):
	return a+b*np.log10(r)

def dindex(r,dr,b):
	return (b*dr)/(r*np.log(10))

def O3N2(Ha,dHa,Hb,dHb,NII,dNII,OIII,dOIII):
	r = (OIII/Hb)*(NII/Ha) 
	dr =( ((1/Hb)*(NII/Ha)*dOIII)**2 +((OIII/Hb)*(1/Ha)*dNII)**2 +
			+((OIII/Hb)*(NII/Ha**2)*dHa)**2 
			+((OIII/Hb**2)*(NII/Ha)*dHb)**2
		   )
	O3N2 = np.log(r)
	dO3N2 = dr/(r*np.log(10))

	met = 8.203+0.630*O3N2-0.327*O3N2**2
	dmet = 0.630-2*0.327*O3N2
	return met,dmet	
