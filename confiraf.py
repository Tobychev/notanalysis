import os
import pdb

modules = ["imutil"]

if not os.path.exists("login.cl"):
    print "No login.cl found in "+os.path.abspath(os.path.curdir)
    print "Attempting to use detfaults from confiraf"
    from pyraf import iraf
    iraf.images(_doprint=0)
    iraf.imutil(_doprint=0)
    iraf.noao(_doprint=0)
    iraf.imred(_doprint=0)
    iraf.ccdred(_doprint=0)
    iraf.onedspec(_doprint=0)
    iraf.twodspec(_doprint=0)
    iraf.longslit(_doprint=0)
    iraf.kpnoslit(_doprint=0)
    iraf.astutil(_doprint=0)
    iraf.images(_doprint=0)
    iraf.imutil(_doprint=0)
else:
    from pyraf import iraf
    #Check that what we need is there
    if iraf.imred.isLoaded() == 0: 
        iraf.imred(_doprint=0)
        iraf.ccdred(_doprint=0)
    elif iraf.ccdred.isLoaded() == 0:
		iraf.ccdred(_doprint=0)

    if iraf.twodspec.isLoaded() == 0:
    	iraf.twodspec(_doprint=0)
    	iraf.longslit(_doprint=0)
    	iraf.apextract(_doprint=0)
    elif iraf.longslit.isLoaded() == 0:
    	iraf.longslit(_doprint=0)
    	iraf.apextract(_doprint=0)
    elif iraf.apextract.isLoaded() == 0:
		iraf.apextract(_doprint=0)

    if iraf.onedspec.isLoaded() == 0:
    	iraf.onedspec(_doprint=0)
