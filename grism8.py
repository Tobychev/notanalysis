import scipy as sp
import pyfits as fits
import fluxcal as fl
import confiraf as ciraf
import pyspeckit as spec
import matplotlib.pyplot as pl
import os

iraf = ciraf.iraf

source = "extracted/grism8/treLARS1gr8.ms.fits"
cal = "instrument/grism8cal.fits"
sci = "science/sciGrism8.fits"

NII = 6778
SIIa = 6914
SIIb = 6929

#If its there, don't make a new one
if not os.path.exists(sci):
    #Multiply by instrument response to get 
    #proper flux units
    iraf.imarith(source,"*",cal,sci)

with fits.open(sci,mode="update") as fil:
    hdr = fil[0].header
    hdr["bunit"] = "erg/s/cm^2/Ang"
    numspec = hdr["naxis2"]

#Hand guessed values
#Range to fit background on
xmin=6620;xmax=7050
#Exclude peaks 
exclude=[6722,6799,6895,6946]



#From trying to fit NII and H-alpha along with that other line
Triguesses = [1.672008680274834e-15, 6738.5591397849466, 0.51141957866758936, 1.243651057208487e-14, 6753.4623655913983, 0.63927447333448673, 5.2793660984784123e-15, 6774.989247311828, 0.38356468400146448]

#For the SII lines
Twoguesses = [1.1526079008732994e-15, 6911.8279569892475, 0.38356468400069199, 1.0565051680407391e-15, 6926.2795698924729, 0.76712936800215648]

#All together!
five =  [1.672008680274834e-15, 6738.5591397849466, 0.51141957866758936, 1.243651057208487e-14, 6753.4623655913983, 0.63927447333448673, 5.2793660984784123e-15, 6774.989247311828, 0.38356468400146448,1.1526079008732994e-15, 6911.8279569892475, 0.38356468400069199, 1.0565051680407391e-15, 6926.2795698924729, 0.76712936800215648]
guess = five
two = Twoguesses
tri = Triguesses
fitres=[]

for I in range(1,13):
    guess = five
    gr8 = spec.Spectrum(sci,specnum=I)
    gr8.baseline(xmin=xmin,xmax=xmax,exclude=exclude,subtract=True,reset_selection=True,order=1)
    gr8.specfit(guesses=two,fittype="gaussian")
    gr8.measure()
    fitres.append( (I,gr8.measurements.lines) )
    gr8.specfit(guesses=tri,fittype="gaussian")
    gr8.measure()
    fitres.append( (I,gr8.measurements.lines) )

fl.amplitude(fitres)
