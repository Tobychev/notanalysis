import confiraf as ciraf
import pyfits as fits
import os

iraf = ciraf.iraf

def make_flat(fromdir,fil,layer,targetdir):
	iraf.imcopy("{0}/{1}[{2:d}]".format(fromdir,fil,layer),"{0}/{1}".format(targetdir,fil))

def flatten(frompaths,layer,targetdir):
	if not os.path.isdir(targetdir):
		os.makedirs(targetdir)
	for path in frompaths:
		(fro,name) = os.path.split(path)
		make_flat(fro,name,layer,targetdir)

def listby(key,value,Dir):
	List = []
	files = os.listdir(Dir)
	for fil in files:
		tmp = fits.getheader("{0}/{1}".format(Dir,fil))
		try:
			if tmp[key] in value:
				try:
					List.append((fil, tmp["DATE-AVG"]))
				except err:
					print fil, "No date"
		except:
			print fil, "No object"
	return List

def listbyobject(Dir,obj):
	return listby("OBJECT",obj,Dir)

def biaslist(Dir):
	return listbyobject(Dir,["DarkZero","bias","Bias"])

def darklist(Dir):
	return listbyobject(Dir,["DarkSixty","dark"])

