import numpy as np
#column name -> column index
ap=0;center=1;cont=2;flux=3;eqw=4;core=5;gfwhm=6;#lfwhm <- never used
Errcenter=8;Errcont=9;Errflux=10;Erreqw=11;Errcore=12;Errgfwhm=13;#Errlfwhm

sci = "science/"
gr7prop ={"delta":1.50642287731171,"start":3752.16821289062} 
gr8prop ={"delta":1.25646507740021,"start":5763.4482421875} 

gr7 = "Gr7_{}.dat"
gr8 = "Gr8_{}.tab" #Different endings for historical reasons
z = 1+0.0295 # SDSS
wavs_r = {"OII":3727,"OIIIa":4363,"Hbeta":4861,"OIIIb":4959,"OIIIc":5007,
		  "Halpha":6563,"NII":6584,"SIIa":6717,"SIIb":6731}
wavs= {"Halpha":6757,"NII":6777,"OIIIc":5150,"OIIIa":4492,
"OII":3839,"Hbeta":5004,"SIIb":6925,"SIIa":6910,"OIIIb":5100}

grism7data = {
	"OII":np.genfromtxt(sci+gr7.format(wavs["OII"]),skip_header=1),
	"OIIIa":np.genfromtxt(sci+gr7.format(wavs["OIIIa"]),skip_header=1),
	"Hbeta":np.genfromtxt(sci+gr7.format(wavs["Hbeta"]),skip_header=1),
	"OIIIb":np.genfromtxt(sci+gr7.format(wavs["OIIIb"]),skip_header=1),
	"OIIIc":np.genfromtxt(sci+gr7.format(wavs["OIIIc"]),skip_header=1),
	"Halpha":np.genfromtxt(sci+gr7.format(wavs["Halpha"]),skip_header=1),
	"NII":np.genfromtxt(sci+gr7.format(wavs["NII"]),skip_header=1),
}
grism8data = {
	"Halpha":np.genfromtxt(sci+gr8.format(wavs["Halpha"]),skip_header=1),
	"NII":np.genfromtxt(sci+gr8.format(wavs["NII"]),skip_header=1),
 "badNII":np.genfromtxt(sci+gr8.format(6737),skip_header=1),
	"SIIa":np.genfromtxt(sci+gr8.format(wavs["SIIa"]),skip_header=1),
	"SIIb":np.genfromtxt(sci+gr8.format(wavs["SIIb"]),skip_header=1),
}

gr7aps = np.genfromtxt("science/APgr7",skip_header=1)
gr8aps = np.genfromtxt("science/APgr8",skip_header=1)




Ha8 = grism8data["Halpha"][:,flux]
NII8 = grism8data["NII"][:,flux]
SIIa = grism8data["SIIa"][:,flux]
SIIb = grism8data["SIIb"][:,flux]
dHa8 = grism8data["Halpha"][:,Errflux]
dNII8 = grism8data["NII"][:,Errflux]
dSIIa = grism8data["SIIa"][:,Errflux]
dSIIb = grism8data["SIIb"][:,Errflux]

OII = grism7data["OII"][:,flux]; OII[OII == 0] = np.nan
dOII = grism7data["OII"][:,Errflux]; dOII[dOII == 0] = np.nan
OIIIa = grism7data["OIIIa"][:,flux]; OIIIa[OIIIa == 0] = np.nan
dOIIIa = grism7data["OIIIa"][:,Errflux]; dOIIIa[dOIIIa == 0] = np.nan
OIIIb = grism7data["OIIIb"][:,flux]; OIIIb[OIIIb == 0] = np.nan
dOIIIb = grism7data["OIIIb"][:,Errflux]; dOIIIb[dOIIIb == 0] = np.nan
OIIIc = grism7data["OIIIc"][:,flux]; OIIIc[OIIIc == 0] = np.nan
dOIIIc = grism7data["OIIIc"][:,Errflux]; dOIIIc[dOIIIc == 0] = np.nan
Hb = grism7data["Hbeta"][:,flux]; Hb[Hb == 0] = np.nan
dHb = grism7data["Hbeta"][:,Errflux]; dHb[dHb == 0] = np.nan
Ha7 = grism7data["Halpha"][:,flux]; Ha7[Ha7 == 0] = np.nan
dHa7 = grism7data["Halpha"][:,Errflux]; dHa7[dHa7 == 0] = np.nan
NII7 = grism7data["NII"][:,flux]; NII7[NII7 == 0] = np.nan
dNII7 = grism7data["NII"][:,Errflux]; dNII7[dNII7 == 0] = np.nan
