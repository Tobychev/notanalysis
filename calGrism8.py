#encoding: utf-8
import confiraf as ciraf
import fluxcal as fl
import numpy as np
import scipy.interpolate as si
import matplotlib.pyplot as pl

iraf = ciraf.iraf

eStd = fl.openExtractStd("hlsp1446spec")
std = fl.openStandard("sp1446-mJy.spec","muJy")

pl.figure()
pl.title("Standard star spectrum")
pl.step(std["lam"],std["flux"]) 
pl.show()

pl.figure()
pl.title("Extracted spectrum")
pl.step(eStd["lam"],eStd["flux"]); 
pl.show()

stan = si.interp1d(std["lam"],std["flux"],kind="cubic")

rng = eStd["flux"] < 0.05 
eStd["flux"][rng] = 1

instrument = {	"lam" : eStd["lam"], 
				"flux": stan(eStd["lam"])/eStd["flux"]}

#pl.title("Instrument response")
#pl.step(instrument["lam"][~rng],instrument["flux"][~rng])
#pl.ylabel(u"(erg/s/cm^2/Å) / (Adu/s/pix)")
#pl.xlabel(u"Å")
#pl.show()

#fl.makeCalFrame("grism8cal",12,instrument)
